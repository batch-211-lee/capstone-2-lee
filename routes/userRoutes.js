const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for user authentication
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//route to set user as admin
router.put("/:userId/setAdmin", auth.verify,(req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.setAdmin(data,req.params).then(resultFromController=>res.send(resultFromController));
});

//Find all users
router.get("/all",(req,res)=>{
	userController.getAll().then(resultFromController=>res.send(resultFromController))
});

//Retrieving specific user
router.get("/:userId",(req,res)=>{
	userController.getUser(req.params).then(resultFromController=>res.send(resultFromController))
});

//Route for checkout
router.post("/checkout", auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		order: req.body,
		quantity: req.body.quantity,
		productId:req.body.productId
	}
	userController.checkOut(data).then(resultFromController=>res.send(resultFromController));
});

//route to retrieve orders
router.get("/:userId/checkout",auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
	}
	userController.getOrders(data,req.params).then(resultFromController=>res.send(resultFromController))
})

module.exports = router;