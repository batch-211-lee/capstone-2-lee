const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Route for adding new products
router.post("/addProduct",auth.verify, (req, res) => {

	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data, req.params).then(resultFromController => res.send(resultFromController))
});

//Route for all products
router.get("/all",(req,res)=>{
	productController.getAll().then(resultFromController=>res.send(resultFromController))
});

//Route for retreiving available products
router.get("/",(req,res)=>{
	productController.getAllAvailable().then(resultFromController=>res.send(resultFromController))
});

//Route for retrieving single product
router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
});

//route for updating a product
router.put("/editProduct/:productId", auth.verify,(req,res)=>{
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data,req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

//Route to archive a product
router.put("/:productId/archive", auth.verify,(req,res)=>{
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data,req.params).then(resultFromController=>res.send(resultFromController));
});

//Route to reactivate product
router.put("/:productId/activate", auth.verify,(req,res)=>{
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.activateProduct(data,req.params).then(resultFromController=>res.send(resultFromController));
});

module.exports = router;