const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName :{
		type: String,
		required:[true, "Product name is required"]
	},

	productDescription : {
		type: String,
		required : [true, "Product Description is required"]
	},

	productPrice : {
		type : String,
		default : 0
	},

	isAvailable : {
		type: Boolean,
		default : true
	},

	isActive : {
		type: Boolean,
		default: true
	},

	createdOn : {
		type: Date,
		default: new Date()
	},

	orders : [
		{
			orderId : {
				type : String,
				required : [true, "Order ID is required"]
			},

			userId : {
				type: String
			},

			quantity : {
				type : Number,
				required : [true, "Cannot have 0 quantity"]
			},

			purchasedOn : {
				type : Date,
				default : new Date()
			},
		}
	]
});

module.exports = mongoose.model("Product", productSchema);