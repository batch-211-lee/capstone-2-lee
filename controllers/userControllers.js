const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")
//Controller for user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		userName : reqBody.userName,
		password : bcrypt.hashSync(reqBody.password,10),
		mobileNumber : reqBody.mobileNumber
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return newUser;
		};
	});
};

//Controller for user authentication
module.exports.loginUser = (reqBody) =>{
	return User.findOne({userName:reqBody.userName}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};

//Route to set users as admin

module.exports.setAdmin = (data,reqParams) =>{
	console.log(reqParams)
	if(data.isAdmin){
	let setAdmin = {
		isAdmin : true
	}
	return User.findByIdAndUpdate(reqParams.userId,setAdmin).then((user,error)=>{
		if(error){
			return false;
		}else{
			return user;
		};
	});
}else {
	return false
}
};

//Retrieving all users
module.exports.getAll = () =>{
	return User.find({}).then(result =>{
		return result
	})
};

//Retrieving a specific user
module.exports.getUser = (reqParams)=>{
	return User.findById(reqParams.userId).then(result=>{
		return result;
	});
};


//Checkout
module.exports.checkOut = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.orders.push({
			// totalAmount : data.totalAmount,
			products:[{
			productId: data.productId, 
			quantity: data.quantity
		}]
	});
		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else {
				true;
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product=>{
		product.orders.push({orderId: product.orders[1].orderId, userId:data.userId,quantity:data.quantity});
		return product.save().then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	if(isUserUpdated&&isProductUpdated){
		return false;
	}else{
		return "Both updated";
	}
}

//retrieve order from authenticated user

module.exports.getOrders = (reqParams)=>{
	return User.findById(reqParams.userId).then(result=>{
		let orders = result.orders;
		console.log(orders)
		return orders
	});
};