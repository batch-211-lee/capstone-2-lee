const Product = require("../models/Product");

module.exports.addProduct = (data) => {

	if (data.isAdmin) {
		let newProduct = new Product({
			productName : data.product.productName,
			productDescription : data.product.productDescription,
			productPrice : data.product.productPrice
		});

		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return newProduct;
			};
		});
	} else {
		return false;
	};
};


//Get all
module.exports.getAll = () =>{
	return Product.find({}).then(result =>{
		return result
	})
}

//Getting all available products
module.exports.getAllAvailable = () =>{
	return Product.find({isAvailable:true}).then(result=>{
		return result;
	});
};

//Retrieving Single product
module.exports.getProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};

//Updating product
module.exports.updateProduct = (data,reqParams,reqBody)=>{
	if(data.isAdmin){
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		productPrice: reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else {
			return product;
		};
	});
	} else{
		return false
	};
};

//Archiving product
module.exports.archiveProduct = (data,reqParams)=>{
	if(data.isAdmin){
	let archivedProduct = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,error)=>{
		if(error){
			return false;
		}else {
			return product;
		};
	});
	} else{
		return false
	};
};

//Re-activating product
module.exports.activateProduct = (data,reqParams)=>{
	if(data.isAdmin){
	let activatedProduct = {
		isActive: true
	};
	return Product.findByIdAndUpdate(reqParams.productId,activatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else {
			return product;
		};
	});
	} else{
		return false
	};
};